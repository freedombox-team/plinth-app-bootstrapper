# SPDX-License-Identifier: AGPL-3.0-or-later
"""
FreedomBox app for $app.
"""

from django.utils.translation import gettext_lazy as _

from plinth import app as app_module
from plinth import frontpage, menu
from plinth.daemon import Daemon
from plinth.modules.apache.components import Webserver
from plinth.modules.backups.components import BackupRestore
from plinth.modules.firewall.components import Firewall
from plinth.package import Packages

from . import manifest


_description = [
    _(''),
]


class $appApp(app_module.App):
    """FreedomBox app for $app."""

    app_id = '$app'

    _version = 1

    def __init__(self):
        """Create components for the app."""
        super().__init__()

        info = app_module.Info(self.app_id, self._version, name=_('$app'),
                               icon_filename='$app',
                               short_description=_(''),
                               description=_description, manual_page='$app',
                               clients=manifest.clients)
        self.add(info)

        menu_item = menu.Menu('menu-$app', info.name, info.short_description,
                              info.icon_filename, '$app:index',
                              parent_url_name='apps')
        self.add(menu_item)

        shortcut = frontpage.Shortcut(
            'shortcut-$app', info.name, info.short_description,
            info.icon_filename, url='/$app', clients=manifest.clients,
            login_required=True)
        self.add(shortcut)

        packages = Packages('packages-$app', ['$app'])
        self.add(packages)

        firewall = Firewall('firewall-$app', info.name, ports=['http', 'https'],
                            is_external=True)
        self.add(firewall)

        webserver = Webserver('webserver-$app', '$app-freedombox',
                              urls=['https://{host}/$app/'])
        self.add(webserver)

        daemon = Daemon('daemon-$app', '$app', listen_ports=[
            (12345, 'tcp4'), (12345, 'tcp6')])
        self.add(daemon)

        backup_restore = BackupRestore('backup-restore-$app',
                                       **manifest.backup)
        self.add(backup_restore)

    def setup(self, old_version=None):
        """Install and configure the app."""
        super().setup(old_version)
        if not old_version:
            self.enable()
