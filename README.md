# Plinth App Bootstrapper
Utility to quickly bootstrap integration of a new service into Plinth.

Plinth App Boostrapper is expected save a few hours' work in starting integration of a new service into Plinth. Eventually, this script would do everything needed to integrate a simple service into Plinth.

# Sample Usage
Clone this repository and run `bootstrap.py` like this to bootstrap a new plinth module:

    user@debian:~/code/plinth-app-bootstrapper$ ./bootstrap.py --plinth-home ~/code/plinth -v myApp 
    Already on 'master'
    Your branch is up to date with 'origin/master'.
    Switched to a new branch 'myApp'
    Creating module-file data/etc/plinth/modules-enabled/myApp
    Creating dummy action script in actions/myApp
    Creating django app structure in plinth/modules/myApp
    Creating (empty) apache configuration in /home/user/code/plinth/data/etc/apache2/conf-available/myApp-plinth.conf
