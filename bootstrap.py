#!/usr/bin/env python3
#
# This file is part of plinth-app-bootstrapper
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Script to quickly bootstrap a new plinth app
"""

import argparse
import os
import subprocess

import cfg

CURR_DIR = os.getcwd()


def parse_arguments():
    """ Return parsed command line arguments as dictionary. """
    parser = argparse.ArgumentParser(
        description='Script to quickly bootstrap a new plinth app',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--verbose',
                        '-v',
                        help="verbose output",
                        action='store_true')
    parser.add_argument('app', help="Name of the new app")
    parser.add_argument('--plinth-home',
                        default=cfg.plinth_home,
                        help="Location of Plinth's repository")
    arguments = parser.parse_args()
    cfg.plinth_home = arguments.plinth_home
    cfg.app = arguments.app
    cfg.verbose = arguments.verbose


def create_git_branch():
    """Checkout a new git branch from master"""
    vprint("Creating new git branch for ${}".format(cfg.app))
    subprocess.call(['git', 'checkout', 'master'])
    subprocess.call(['git', 'checkout', '-b', cfg.app])


def enable_module():
    """Add app to enabled modules"""
    modules_enabled_dir = os.path.join(
        'plinth/modules', cfg.app, 'data/usr/share/freedombox/modules-enabled')
    os.path.exists(modules_enabled_dir) or os.makedirs(modules_enabled_dir)
    with open(os.path.join(modules_enabled_dir, cfg.app), 'w') as f:
        vprint("Creating module-file %s" % f.name)
        f.write(".".join(["plinth.modules", cfg.app]))
        f.write("\n")


def create_django_app():
    """Create a new django app for the new plinth app"""
    django_app_dir = os.path.join("plinth", "modules", cfg.app)
    vprint("Creating django app structure in %s" % django_app_dir)
    os.path.exists(django_app_dir) or os.makedirs(django_app_dir)

    module_path = os.path.join(CURR_DIR, "templates", "module")

    for fil in os.listdir(module_path):
        with open(os.path.join(module_path, fil), 'r') as f:
            template = f.read()

        with open(os.path.join(django_app_dir, fil), 'w') as f:
            f.write(template.replace("$app", cfg.app))


def create_apache_config():
    """Create apache configuration"""
    conf_dir = os.path.join(
        'plinth/modules', cfg.app,
        'data/usr/share/freedombox/etc/apache2/conf-available')
    os.path.exists(conf_dir) or os.makedirs(conf_dir)
    path = os.path.join(conf_dir, "{}-freedombox.conf".format(cfg.app))
    vprint("Creating (empty) apache configuration in %s" % path)
    os.path.exists(path) or subprocess.call(['touch', path])


def vprint(message):
    """ Print a message when in verbose mode """
    if cfg.verbose:
        print(message)


def bootstrap():
    os.chdir(cfg.plinth_home)

    create_git_branch()

    create_django_app()

    enable_module()

    create_apache_config()


def main():
    """Parse arguments and perform all duties."""
    parse_arguments()
    bootstrap()


if __name__ == '__main__':
    main()
